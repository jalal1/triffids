import {
    parkService
} from '../services/Park.service'

export default {
    namespaced: true,

    state: {
        parkList: [], // [{'id': 'ADELPL', 'sitename': 'Adelaide Place Park'}...]
        currentParkId: '',
        currentPark: {},
    },

    getters: {
        parkList: (st) => st.parkList,
        currentParkId: (st) => st.currentParkId,
    },

    actions: {
        async getParkList(context) {
            if (context.state.parkList.length === 0) {
                // load the park list
                const parks = await parkService.parkList()
                context.commit('setParkList', parks)
            }
            return context.getters.parkList
        },
    },

    mutations: {
        setParkList(st, parks) {
            st.parkList = parks
        },
        setCurrentParkId(st, id) {
            st.currentParkId = id
        },
    },

}