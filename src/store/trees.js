import Vue from 'vue'
import {
    treeService
} from '../services/Tree.service'

export default {
    namespaced: true,

    state: {
        parkId: '',
        trees: [],
        currentTree: {},
    },

    getters: {
        getTrees: (st) => JSON.parse(JSON.stringify(st.trees)),
        getCurrentTree: (st) => st.currentTree,
    },

    mutations: {
        setParkId: (st, id) => {
            st.parkId = id
        },
        setTrees: (st, trees) => {
            st.trees = trees
        },
        setCurrentTree: (st, tree) => {
            st.currentTree = tree
        },
    },

    actions: {
        async tree(context, treeId) { // currently just a pass-through
            const data = await treeService.tree(treeId)
            context.commit('setCurrentTree', data)
            return data
        },
        async trees(context, parkId) {
            if (context.state.parkId !== parkId) {
                // new park, reload trees
                context.commit('setParkId', parkId)
                const data = await treeService.trees(parkId)
                const trees = data.map(val => {
                    return {
                        id: val.recordid,
                        name: val.fields.common_name,
                        full_name: val.fields.full_common_name,
                        girth: val.fields.dbh,
                        width: val.fields.crown_area,
                        height: val.fields.crown_height,
                        latin: val.fields.latin_name,
                        latin_code: val.fields.latin_code,
                        geo_point: {
                            lat: val.fields.geo_point_2d[0],
                            lng: val.fields.geo_point_2d[1]
                        }
                    };
                })
                context.commit('setTrees', trees)
                Vue.$log.info('store/trees: trees: new park: ', parkId)
            } else { // same park
                if (context.state.trees.length === 0) {
                    context.commit('setParkId', parkId)
                    const data = await treeService.trees(parkId)
                    const trees = data.map(val => {
                        return {
                            id: val.recordid,
                            name: val.fields.common_name,
                            full_name: val.fields.full_common_name,
                            girth: val.fields.dbh,
                            width: val.fields.crown_area,
                            height: val.fields.crown_height,
                            latin: val.fields.latin_name,
                            latin_code: val.fields.latin_code,
                            geo_point: {
                                lat: val.fields.geo_point_2d[0],
                                lng: val.fields.geo_point_2d[1]
                            }
                        };
                    })
                    context.commit('setTrees', trees)
                    Vue.$log.info('store/trees: trees: loading park: ', parkId)
                } else {
                    Vue.$log.info('store/trees: trees: cached trees for: ', parkId)
                }
            }
            // return context.getters['getTrees']
        }
    },

}